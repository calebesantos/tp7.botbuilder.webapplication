/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.infnet.botbuilder;

/**
 * 
 * @author Calebe Santos
 */
public class PlayerManager {
    private static int countPlayersWaiting;
    private static int countPlayersReady;
    
    public static void addPlayer() {
        countPlayersWaiting++;
    }
    
    public static void resetPlayer() {
        countPlayersWaiting = 0;
    }
    
    public static int getCountPlayer() {
        return countPlayersWaiting;
    }
    
    public static void addPlayerReady() {
        countPlayersReady++;
    }
    
    public static void resetPlayerReady() {
        countPlayersReady = 0;
    }
    
    public static int getCountPlayerReady() {
        return countPlayersReady;
    }
}
