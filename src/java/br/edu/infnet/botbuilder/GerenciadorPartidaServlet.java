/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.infnet.botbuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Calebe Santos
 */
@WebServlet(name = "GerenciadorPartidaServlet", urlPatterns = {"/GerenciadorPartidas"})
public class GerenciadorPartidaServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet GerenciadorPartidaServlet</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet GerenciadorPartidaServlet at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }

//        request.getRequestDispatcher("views/user/main.html").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
//        response.setContentType("application/json");
//// Get the printwriter object from response to write the required json object to the output stream      
//        PrintWriter out = response.getWriter();
//// Assuming your json object is **jsonObject**, perform the following, it will return your json object  
//        out.print("{\"teste\":\"teste\"}");
//        out.flush();

        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String json = "";
        if (br != null) {
            json = br.readLine();
        }

        String status = json.substring(11, json.lastIndexOf("\"}"));

        if (status.equals("solicitado")) {
            PlayerManager.addPlayer();
            status = "fila";

        } else if (status.equals("fila")) {
            if (PlayerManager.getCountPlayer() == 4) {
                status = "pronto";
            }
        } else if (status.equals("pronto")) {
            PlayerManager.addPlayerReady();
            status = "esperando";
        } else if (status.equals("esperando")) {
            if (PlayerManager.getCountPlayerReady() == 4) {
                response.sendRedirect("IniciarPartida");
            }
        }

        response.getWriter().write("{\"status\":\"" + status + "\"}");
//        request.getRequestDispatcher("/index.html").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
